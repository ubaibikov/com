package main

import (
	"flag"
	"log"

	"github.com/BurntSushi/toml"
	"github.com/com/cmd/apiserver"
	"github.com/com/internal/app"
	"github.com/com/store"
)

var (
	configPath string
)

func init() {
	flag.StringVar(&configPath, "config-path", "../configs/apiserver.toml", "path to config file")
	flag.Parse()
}

func main() {
	config := parceConfig()

	store := store.New(config.DBUrl)
	app := app.New(config, store)
	if err := app.Start(); err != nil {
		panic(err)
	}
}

func parceConfig() *apiserver.Config {
	initConf := apiserver.InitConfig()

	if _, err := toml.DecodeFile(configPath, initConf); err != nil {
		log.Fatal(err.Error())
	}

	return initConf
}
