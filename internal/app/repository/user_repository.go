package repository

import (
	"crypto/md5"
	"database/sql"
	"encoding/hex"

	"github.com/com/internal/app/model"
)

// UserRepository ..
type UserRepository struct {
	DB *sql.DB
}

// Create User
func (r *UserRepository) Create(u *model.User) (*model.User, error) {
	err := r.DB.QueryRow("insert into users(email, password) values($1,$2) returning id", u.Email, hashPassword(u.Password)).Scan(&u.ID)
	if err != nil {
		return nil, err
	}

	return u, nil
}

func hashPassword(password string) string {
	hash := md5.Sum([]byte("ultra_secret+_token" + password))
	return hex.EncodeToString(hash[:])
}

//FindByEmail ...
func (r *UserRepository) FindByEmail(email string) (int, error) {
	var count int

	err := r.DB.QueryRow("select count(1) from users where email=$1", email).Scan(&count)
	if err != nil {
		return 0, err
	}

	return count, nil
}

// FindUser ...
func (r *UserRepository) FindUser(email string, password string) (int, error) {
	var count int

	err := r.DB.QueryRow("select count(1) from users where email=$1 and password=$2", email, hashPassword(password)).Scan(&count)
	if err != nil {
		return 0, err
	}

	return count, nil
}

//GetAll ...
func (r *UserRepository) GetAll() ([]*model.User, error) {
	rows, err := r.DB.Query("select * from users")
	if err != nil {
		return nil, err
	}
	users := make([]*model.User, 0)

	for rows.Next() {
		user := new(model.User)
		if err := rows.Scan(&user.ID, &user.Email, &user.Password); err != nil {
			return nil, err
		}
		users = append(users, user)
	}

	return users, nil
}
