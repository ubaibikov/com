package app

import "github.com/gin-gonic/gin"

func userAuth(c *gin.Context) {
	if _, err := c.Cookie("auth_key"); err != nil {
		c.JSON(401, gin.H{
			"message": "Вы не авторизован",
		})

		return
	}

	c.Next()
}
