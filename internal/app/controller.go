package app

import (
	"log"

	"github.com/com/internal/app/model"
	"github.com/com/tools"
	"github.com/gin-gonic/gin"
)

//Index ...
func (s *Server) index(c *gin.Context) {
	users, err := s.Store.User().GetAll()
	if err != nil {
		log.Fatal(err)
	}

	c.JSON(200, gin.H{
		"message": "пользователи",
		"users":   users,
	})
}

// Register ..
func (s *Server) register(c *gin.Context) {
	email := c.PostForm("email")
	password := c.PostForm("password")
	count, err := s.Store.User().FindByEmail(email)
	if err != nil {
		panic(err)
	}

	if count > 0 {
		c.JSON(401, gin.H{
			"message": "Пользователь с таким мылом уже существует",
		})
		return
	}

	if _, err := s.Store.User().Create(&model.User{Email: email, Password: password}); err != nil {
		log.Fatal(err)
	}

	generetedToken := tools.GenerateToken()
	//  set user token
	c.SetCookie(
		"auth_key",
		generetedToken,
		3600,
		"/",
		s.Config.Host,
		false,
		false,
	)

	c.JSON(200, gin.H{
		"auth_key": generetedToken,
	})
}

// Login ...
func (s *Server) login(c *gin.Context) {

	email := c.PostForm("email")
	password := c.PostForm("password")

	count, err := s.Store.User().FindUser(email, password)
	if err != nil {
		log.Fatal(err)
	}

	if count == 0 {
		c.JSON(401, gin.H{
			"message": "Такого юсера и в помине нет.",
		})
		return
	}
	generetedToken := tools.GenerateToken()

	//  set user token
	c.SetCookie(
		"auth_key",
		generetedToken,
		3600,
		"/",
		s.Config.Host,
		false,
		false,
	)

	c.JSON(200, gin.H{
		"auth_key": generetedToken,
	})

}
